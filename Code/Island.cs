﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MGen
{
    public struct Island
    {
        public List<Point2> Tiles { get; }

        public Island(MapTerrainType[,] terrain, HashSet<Point2> terrainTiles, Predicate<MapTerrainType> cond)
        {
            var tileStack = new Stack<Point2>();

            //get the tiles the first one of this island can reach
            var reachable = FloodFill.Get(terrain, terrainTiles.First(), cond, false);

            //remove them all the tiles passed from the terrain
            foreach(var t in reachable)
            {
                terrainTiles.Remove(t);
                tileStack.Push(t);
            }

            //add to my list
            Tiles = tileStack.ToList();

            //Godot.GD.Print("Created: ", Tiles.Count);
        }
    }
}
