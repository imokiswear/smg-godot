﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace MGen
{
    public static class FloodFill
    {
        /// <summary>
        /// Get the surroudings based on the condition.
        /// </summary>
        public static List<Point2> Get<T>(T[,] a, Point2 p, Predicate<T> condition, bool allowDiagonals)
        {
            var width = a.GetLength(0);
            var height = a.GetLength(1);

            //create a 2D array w/ the size of the one we passed
            bool[,] hasBeenChecked = new bool[width, height];

            //if the starting point doesn't meet the condition, stop it here
            if (!condition(a[p.X, p.Y]))
            {
                return new List<Point2>();
            }

            //list of objects that will check neighbours and possibly add them to the open list -- add the starting point to it
            var open = new Stack<Point2>(a.Length);
            open.Push(p);

            //list that will be returned
            var closed = new List<Point2>(a.Length);
            Point2[] neighbours;

            //Save to a list and iterate it using a foreach to check the neighbours
            if (allowDiagonals)
            {
                neighbours = new Point2[8]
                {
                    new Point2( 1,  0),
                    new Point2(-1,  0),
                    new Point2( 0,  1),
                    new Point2( 0, -1),
                    new Point2( 1,  1),
                    new Point2(-1,  1),
                    new Point2( 1, -1),
                    new Point2(-1, -1)
                };
            }
            else
            {
                neighbours = new Point2[4]
                {
                    new Point2( 1,  0),
                    new Point2(-1,  0),
                    new Point2( 0,  1),
                    new Point2( 0, -1)
                };
            }

            //set the starting point as checked
            hasBeenChecked[p.X, p.Y] = true;

            while (open.Count > 0)
            {
                //get the first panel of the open list, remove it -- then, add to the list we're gonna return
                p = open.Pop();
                closed.Add(p);

                //check its neighbours
                for (var i = 0; i < neighbours.Length; i++)
                {
                    var k = new Point2(p.X + neighbours[i].X, p.Y + neighbours[i].Y); //neighbour = current + offset

                    if (k.X < 0 || k.X >= width || k.Y < 0 || k.Y >= height)
                        continue;

                    if (hasBeenChecked[k.X, k.Y] || !condition(a[k.X, k.Y]))
                        continue;

                    open.Push(k);
                    hasBeenChecked[k.X, k.Y] = true;
                }
            }

            closed.TrimExcess();
            return closed;
        }

        /// <summary>
        /// Fill the surroudings based on the condition.
        /// </summary>
        public static void Fill<T>(T[,] array, Point2 p, Predicate<T> condition, bool allowDiagonals, T value)
        {
            var width = array.GetLength(0);
            var height = array.GetLength(1);

            //create a 2D array w/ the size of the one we passed
            bool[,] hasBeenChecked = new bool[width, height];

            //if the starting point doesn't meet the condition, stop it here
            if (!condition(array[p.X, p.Y]))
            {
                return;
            }



            //list of objects that will check neighbours and possibly add them to the open list -- add the starting point to it
            var open = new Stack<Point2>(array.Length);
            open.Push(p);

            //list that will be returned
            //var closed = new List<Point2>(a.Length);
            Point2[] neighbours;

            //Save to a list and iterate it using a foreach to check the neighbours
            if (allowDiagonals)
            {
                neighbours = new Point2[8]
                {
                    new Point2( 1,  0),
                    new Point2(-1,  0),
                    new Point2( 0,  1),
                    new Point2( 0, -1),
                    new Point2( 1,  1),
                    new Point2(-1,  1),
                    new Point2( 1, -1),
                    new Point2(-1, -1)
                };
            }
            else
            {
                neighbours = new Point2[4]
                {
                    new Point2( 1,  0),
                    new Point2(-1,  0),
                    new Point2( 0,  1),
                    new Point2( 0, -1)
                };
            }

            //set the starting point as checked
            hasBeenChecked[p.X, p.Y] = true;

            while (open.Count > 0)
            {
                //get the first panel of the open list, remove it -- then, add to the list we're gonna return
                p = open.Pop();

                array[p.X, p.Y] = value;

                //check its neighbours
                for (var i = 0; i < neighbours.Length; i++)
                {
                    var k = new Point2(p.X + neighbours[i].X, p.Y + neighbours[i].Y); //neighbour = current + offset

                    if (k.X < 0 || k.X >= width || k.Y < 0 || k.Y >= height)
                        continue;

                    if (hasBeenChecked[k.X, k.Y] || !condition(array[k.X, k.Y]))
                        continue;

                    open.Push(k);
                    hasBeenChecked[k.X, k.Y] = true;
                }
            }
        }
    }
}
