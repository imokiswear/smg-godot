﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Godot;

namespace MGen
{
    public struct Point2
    {
        public int X { get; }
        public int Y { get; }

        public Point2(int x, int y)
        {
            X = x;
            Y = y;
        }
        public Point2(float x, float y)
        {
            X = (int)Math.Floor(x);
            Y = (int)Math.Floor(y);
        }
        public Point2(Point2 p)
        {
            X = p.X;
            Y = p.Y;
        }
        public Point2(Vector2 p)
        {
            X = (int)p.x;
            Y = (int)p.y;
        }

        public static Point2 One => new Point2(1, 1);
        public static Point2 UnitX => new Point2(1, 0);
        public static Point2 UnitY => new Point2(0, 1);

        public static int AngleBetween(Point2 p1, Point2 p2)
        {
            return (int)(Math.Atan2(p2.Y - p1.Y, p2.X - p1.X) * 180 / Math.PI) + 90;
        }
        public static int Manhattan(Point2 p1, Point2 p2)
        {
            var diff = p1 - p2;
            return Math.Abs(diff.X) + Math.Abs(diff.Y);
        }
        
        public static explicit operator Vector2(Point2 p)
        {
            return new Vector2(p.X, p.Y);
        }
        public static implicit operator Point2(Vector2 v)
        {
            return new Point2(v.x, v.y);
        }

        public static Point2 operator -(Point2 p1, Point2 p2)
        {
            return new Point2(p1.X - p2.X, p1.Y - p2.Y);
        }
        public static Point2 operator -(Point2 p1, int v)
        {
            return new Point2(p1.X - v, p1.Y - v);
        }
        public static Point2 operator -(Point2 p1)
        {
            return new Point2(-p1.X, -p1.Y);
        }
        public static Point2 operator *(Point2 p1, int v)
        {
            return new Point2(p1.X * v, p1.Y * v);
        }
        public static Point2 operator /(Point2 p1, int v)
        {
            return new Point2(p1.X / v, p1.Y / v);
        }
        public static Point2 operator +(Point2 p1, Point2 p2)
        {
            return new Point2(p1.X + p2.X, p1.Y + p2.Y);
        }
        public static Point2 operator +(Point2 p1, int v)
        {
            return new Point2(p1.X + v, p1.Y + v);
        }
        
        public override string ToString()
        {
            return "(" + X + ", " + Y + ")";
        }

        public Vector2 ToVec2()
        {
            return new Vector2(X, Y);
        }
    }
}
