using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using Godot;
using System.Diagnostics;
namespace MGen
{
    public enum MapTerrainType { DeepWater, Water, Sand, Grass, Hill, HighHill }

    public class Map : TileMap
    {
        [Export]
        int brushSize = 1;

        [Export]
        int cycles = 4, repsPerCycle = 20;

        [Export]
        int islandTilesThreshold = 8;

        List<Island> islands;

        [Export]
        bool isWorldInfinite;

        int landmass;

        int lastX, lastY;

        [Export]
        Vector2 mapSize = new Vector2(64, 64);
        [Export]
        int minBrushSize = 2, maxBrushSize = 5;
        [Export]
        int posVar = 5;

        RandomNumberGenerator rng;

        List<Point2> walkable;
        public int[,] Heightmap { get; set; }

        public Dictionary<MapTerrainType, int> SetAfter { get; set; } = new Dictionary<MapTerrainType, int>()
        {
            { MapTerrainType.HighHill, 78 },
            { MapTerrainType.Hill, 40 },
            { MapTerrainType.Grass, 16 },
            { MapTerrainType.Sand, 9 },
            { MapTerrainType.Water, 0 },
            { MapTerrainType.DeepWater, -1 }
        };

        public MapTerrainType[,] Terrain { get; set; }
        public int Height { get; set; }
        public int Width { get; set; }

        public override void _Input(InputEvent e)
        {
            /*
            if (e.IsActionPressed("One"))
            {
                var mPos = WorldToMap(GetGlobalMousePosition());
                var mX = (int)mPos.x;
                var mY = (int)mPos.y;
                var s = Stopwatch.StartNew();

                var a = GetIslandMass(new Point2(mX, mY), isWorldInfinite);
                s.Stop();

                GetNode<Label>("../canvas/msg").Text = $"Breadth: { a.Count } -- Time: { s.ElapsedMilliseconds }ms";
            }

            if (e.IsActionPressed("Two"))
            {
                var mPos = WorldToMap(GetGlobalMousePosition());
                var mX = (int)mPos.x;
                var mY = (int)mPos.y;

                var s = Stopwatch.StartNew();
                
                var a = GetIslandMass2(new Point2(mX, mY), isWorldInfinite);
                s.Stop();

                foreach(var i in a)
                {
                    SetCell(i.X, i.Y, (int)MapTerrainType.HighHill);
                }

                GetNode<Label>("../canvas/msg").Text = $"Breadth: { a.Count } -- Time: { s.ElapsedMilliseconds }ms";
            }

    */

        }
        public override void _Process(float delta)
        {
            if (Input.IsActionPressed("mouseLMB"))
            {
                var mPos = WorldToMap(GetGlobalMousePosition());
                var mX = (int)mPos.x;
                var mY = (int)mPos.y;

                if ((mX != lastX || mY != lastY) && IsWithin(mX, mY))
                {
                    PaintCircleAt((int)mPos.x, (int)mPos.y, brushSize - 1, 3, isWorldInfinite, true);
                }

                lastX = (int)mPos.x;
                lastY = (int)mPos.x;
            }

            if (Input.IsActionPressed("mouseRMB"))
            {
                var mPos = WorldToMap(GetGlobalMousePosition());
                var mX = (int)mPos.x;
                var mY = (int)mPos.y;

                if ((mX != lastX || mY != lastY) && IsWithin(mX, mY))
                {
                    PaintCircleAt((int)mPos.x, (int)mPos.y, brushSize - 1, -3, isWorldInfinite, true);
                }

                lastX = (int)mPos.x;
                lastY = (int)mPos.x;
            }
        }

        public override void _Ready()
        {
            foreach(var v in SetAfter)
            {
                GD.Print(v.ToString());
            }

            rng = new RandomNumberGenerator();
            //rng.Randomize();

            Generate();
        }

        public int GetSA(int type)
        {
            return SetAfter[(MapTerrainType)type];
        }

        public void SetSA(int type, int value)
        {
            SetAfter[(MapTerrainType)type] = value;
        }

        void Generate()
        {
            var s = Stopwatch.StartNew();

            Clear();

            Width = (int)mapSize.x;
            Height = (int)mapSize.y;

            //
            Heightmap = new int[Width, Height];
            Terrain = new MapTerrainType[Width, Height];

            //this is the whole process of generating a terrain
            for (var i = 0; i < cycles; i++)
            {
                var x = rng.RandiRange(0, Width - 1);
                var y = rng.RandiRange(0, Height - 1);

                for (var j = 0; j < repsPerCycle; j++)
                {
                    PaintCircleAt(x, y, rng.RandiRange(minBrushSize, maxBrushSize), 3, isWorldInfinite, false);

                    x += rng.RandiRange(-posVar, posVar);
                    y += rng.RandiRange(-posVar, posVar);
                }
            }

            //
            UpdateMap(false);

            var tilesByType = new Dictionary<MapTerrainType, List<Point2>>();

            foreach(MapTerrainType t in Enum.GetValues(typeof(MapTerrainType)))
            {
                tilesByType[t] = new List<Point2>();
            }


            //get every single walkable (sand | grass) tile and add to a stack
            var walkable = new HashSet<Point2>();
            var highHills = new HashSet<Point2>();
            var hills = new HashSet<Point2>();


            for (var x = 0; x < Width; x++)
            {
                for (var y = 0; y < Height; y++)
                {
                    var p = new Point2(x, y);
                    var t = Terrain[x, y];

                    //tilesByType[p].Add(new Point2(x, y));

                    if (t == MapTerrainType.HighHill)
                    {
                        highHills.Add(p);
                    }
                    else if (t == MapTerrainType.Hill)
                    {
                        hills.Add(p);
                    }
                    else if (t == MapTerrainType.Grass || t == MapTerrainType.Sand)
                    {
                        walkable.Add(p);
                    }
                }
            }
            
            var landmass = walkable.Count;
            
            
            //fix high hills
            while (highHills.Count > 0)
            {
                var i = new Island(Terrain, highHills, t => t == MapTerrainType.HighHill);

                //don't consider anything w/ less than 8 tiles a high hill, turn into a hill
                if (i.Tiles.Count < 8)
                {
                    foreach (var t in i.Tiles)
                    {
                        Terrain[t.X, t.Y] = MapTerrainType.Hill;
                        hills.Add(t);
                    }
                }
            }

            //fix hills
            while (hills.Count > 0)
            {
                var i = new Island(Terrain, hills, t => t == MapTerrainType.Hill);

                //don't consider anything w/ less than 8 tiles a hill, turn into grass
                if (i.Tiles.Count < 8)
                {
                    foreach (var t in i.Tiles)
                    {
                        Terrain[t.X, t.Y] = MapTerrainType.Grass;
                        walkable.Add(t);
                    }
                }
            }

    
            //GD.Print("walkable: ", walkable.Count);

            islands = new List<Island>();

            //here we get the islands; they will use flood fill to check reachable tiles and remove them from the stack
            //at some point the stack will be empty and we will be able to know the no. of islands
            while(walkable.Count > 0)
            {
                var i = new Island(Terrain, walkable, t => t == MapTerrainType.Sand || t == MapTerrainType.Grass);

                //don't consider anything w/ less than <islandTilesThreshold> tiles a island
                if (i.Tiles.Count >= islandTilesThreshold)
                {
                    islands.Add(i);
                }
            }

            DrawMap();
            s.Stop();

            GetNode<Label>("../canvas/msg").Text = $"--Map info--\nWalkable tiles: { landmass }\nIslands (anything w/ { islandTilesThreshold }+ tiles breadth): { islands.Count }\nTime taken: {s.ElapsedMilliseconds}ms";
            //GD.Print("Map generated -- Landmass: ", landmass, " -- Islands: ", islands.Count);
        }

        Point2 GetAdjustedPosition(Point2 p)
        {
            var x = p.X;
            var y = p.Y;

            while (x >= Width)
            {
                x -= Width;
            }
            while (x < 0)
            {
                x += Width;
            }

            while (y >= Height)
            {
                y -= Height;
            }
            while (y < 0)
            {
                y += Height;
            }

            return new Point2(x, y);
        }

        List<Point2> GetIslandMass(Point2 p, bool isWorldInfinite)
        {
            return FloodFill.Get(Terrain, new Point2(p.X, p.Y), t => t == MapTerrainType.Grass || t == MapTerrainType.Sand, false);
        }
        List<Point2> GetIslandMass2(Point2 p, bool isWorldInfinite)
        {
            return FloodFill.Get(Terrain, new Point2(p.X, p.Y), t => t == MapTerrainType.Grass || t == MapTerrainType.Sand, false);
        }

        bool IsLand(Point2 p, bool isWorldInfinite)
        {
            if (isWorldInfinite)
            {
                var aP = GetAdjustedPosition(p);

                return Terrain[aP.X, aP.Y] == MapTerrainType.Grass || Terrain[aP.X, aP.Y] == MapTerrainType.Sand;
            }
            else
            {
                return IsWithin(p.X, p.Y) ? Terrain[p.X, p.Y] == MapTerrainType.Grass || Terrain[p.X, p.Y] == MapTerrainType.Sand : false;
            }
        }
        bool IsWithin(Point2 p)
        {
            return (p.X >= 0 && p.Y >= 0 && p.X < Width && p.Y < Height);
        }
        bool IsWithin(int x, int y)
        {
            return (x >= 0 && y >= 0 && x < Width && y < Height);
        }
        void PaintAt(int x, int y, int value, bool isInfinite = false, bool updateMap = false)
        {
            if (isInfinite)
            {
                while (x >= Width)
                {
                    x -= Width;
                }
                while (x < 0)
                {
                    x += Width;
                }

                while (y >= Height)
                {
                    y -= Height;
                }
                while (y < 0)
                {
                    y += Height;
                }

            }
            else
            {
                if (x < 0 || x >= Width)
                {
                    return;
                }

                if (y < 0 || y >= Height)
                {
                    return;
                }
            }

            Heightmap[x, y] = Mathf.Max(0, Heightmap[x, y] + value);

            if (updateMap)
            {
                UpdateCell(x, y, updateMap);
            }
        }

        void PaintCircleAt(int x, int y, int radius, int value, bool isInfinite = false, bool updateMap = false)
        {

            for (var pX = x - radius; pX < x + radius + 1; pX++)
            {
                for (var pY = y - radius; pY < y + radius + 1; pY++)
                {
                    var d = new Vector2(x - pX, y - pY);

                    if (d.LengthSquared() <= radius * radius)
                    {
                        PaintAt(pX, pY, value, isInfinite, updateMap);
                    }
                }
            }
        }

        void UpdateCell(int x, int y, bool setCell)
        {
            foreach(var after in SetAfter)
            {
                if (Heightmap[x, y] > after.Value)
                {
                    Terrain[x, y] = after.Key;
                    break;
                }
            }

            if (setCell)
                SetCell(x, y, (int)Terrain[x, y]);
        }

        void UpdateMap(bool setCell)
        {
            //landmass = 0;

            for (var x = 0; x < Width; x++)
            {
                for (var y = 0; y < Height; y++)
                {
                    UpdateCell(x, y, setCell);
                }
            }
        }

        void DrawMap()
        {
            //landmass = 0;

            for (var x = 0; x < Width; x++)
            {
                for (var y = 0; y < Height; y++)
                {
                    SetCell(x, y, (int)Terrain[x, y]);
                }
            }
        }
    }

    

}
