extends TileMap

export var width  = 64
export var height = 64

const T_DEEP_WATER = 0
const T_WATER = 1
const T_SAND = 2
const T_GRASS = 3
const T_HILL = 4

const T_HIGH_HILL = 5

var content = []

export var repeats = 8
export var size_base = 5
export var repeats_per_chunk = 5
export var size_var = 2
export var pos_var = 3

export var min_brush_size = 1
export var max_brush_size = 6


func _ready():
	randomize()

func do_it(is_infinite: bool):
	init_content()
	update_map()
	
	var r = 5
	
	for j in repeats:
		var x = int(rand_range(0, width))
		var y = int(rand_range(0, height))
		
		for i in repeats_per_chunk:
			if is_infinite:
				paint_circle_inf(x, y, int(rand_range(min_brush_size, max_brush_size)), 3)
			else:
				paint_circle(x, y, int(rand_range(min_brush_size, max_brush_size)), 3)
				
			x += rand_range(-pos_var, pos_var)
			y += rand_range(-pos_var, pos_var)
			$"../camera".force_update_scroll()
#
#			if r <= 0:
#				yield(get_tree(), "idle_frame")
#				r = 30
#
#			r -= 1
	
	var landmass = get_landmass()
	
	$"../canvas/box/size/value".text = str(width, "x", height)
	$"../canvas/box/landmass/value".text = str(landmass.size())

func bfs(x, y):
	var open = []
	var closed = []
	
	open.append(Vector2(x, y))
	
	while open.size() > 0:
		
		
		
		closed.append

func get_land_neighbours(x, y):
	
	var neighbours = []
	
	if x < width - 1 and is_land(x + 1, y):
		neighbours.append(Vector2(x + 1, y))
	
	if x >= 1 and is_land(x - 1, y):
		neighbours.append(Vector2(x - 1, y))
	
	
	if y >= 1 and is_land(x, y - 1):
		neighbours.append(Vector2(x - 1, y))
	
	
		
		
		
	
	

func is_land(x, y):
	var v = content[x][y]
	
	return v == T_GRASS or T_SAND

func get_content_inf(x, y):
	while x < 0:
		x += width
		
	while x >= width:
		x -= width
	
	while y < 0:
		y += height
		
	while y >= height:
		y -= height
	
	return content[x][y]

func get_landmass():
	var v = []
	
	for x in width:
		for y in height:
			var c = content[x][y]
			
			if c == T_GRASS or c == T_SAND:
				v.append(Vector2(x, y))
	
	return v

func _input(e):
	if e.is_action_pressed("ui_up"):
		do_it(false)
	
	if e.is_action_pressed("ui_down"):
		do_it(true)

func paint(x: int, y: int, value: int):
	if x < 0 or x >= width:
		return
	
	if y < 0 or y >= height:
		return
	
	content[x][y] += value

	
func paint_and_update_inf(x: int, y: int, value: int):
	while x < 0:
		x += width
		
	while x >= width:
		x -= width
	
	while y < 0:
		y += height
		
	while y >= height:
		y -= height
	
	content[x][y] += value
	update_cell(x, y)
	
func paint_and_update(x: int, y: int, value: int):
	if x < 0 or x >= width:
		return
	
	if y < 0 or y >= height:
		return
	
	content[x][y] += value
	update_cell(x, y)

func paint_circle(x: int, y: int, r: int, value: int):
	for _x in range(x - r, x + r + 1):
		for _y in range(y - r, y + r + 1):
			var d = Vector2(x - _x, y - _y)
			
			if d.length_squared() <= r * r:
				#paint(_x, _y, value)
				paint_and_update(_x, _y, value)

func paint_circle_inf(x: int, y: int, r: int, value: int):
	for _x in range(x - r, x + r + 1):
		for _y in range(y - r, y + r + 1):
			var d = Vector2(x - _x, y - _y)
			
			if d.length_squared() <= r * r:
				paint_and_update_inf(_x, _y, value)



func update_map():
	for x in range(width):
		for y in range(height):
			update_cell(x, y)

func update_cell(x: int, y: int):
	var c = content[x][y]
	var v = 0
	
	if c > 78:
		v = T_HIGH_HILL
		
	elif c > 40:
		v = T_HILL
		
	elif c > 16:
		v = T_GRASS
		
	elif c > 9:
		v = T_SAND
		
	elif c > 0:
		v = T_WATER
		
	else:
		v = T_DEEP_WATER
		
	set_cell(x, y, v)

func init_content():
	content = []
	
	for x in range(width):
		content.append([])
		
		for y in range(height):
			content[x].append(0)